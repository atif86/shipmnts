import React from 'react';
import { expect } from 'chai';
import { describe, it } from 'mocha';
import { shallow, mount, render } from 'enzyme';
import sinon from 'sinon';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import shipmntsApp from '../reducers';
import App from '../containers/App'
import Sidebar from '../components/Sidebar';
import Page from '../components/Page';
import InputDetails from '../components/InputDetails';
import Editor from '../components/Editor';

import { actions as shipmntsActions, types as shipmntsTypes }from '../reducers/shipmntsApp'

const store = createStore(shipmntsApp)

function setup(){
  const props = {
    showTemplate: false,
    showDataMessage: false,
    currentKey: '1',
  };

  const enzymeWrapper = shallow(
    <Provider store={store}>
      <App {...props} />
    </Provider>
  )

  return {
    props,
    enzymeWrapper,
  }
}

describe('<App />', () => {

  it('renders itself', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.contains(<App showTemplate={false} showDataMessage={false} currentKey="1" />)).to.equal(true);
  });

  it('check props', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.props().showTemplate).to.equal(false);
    expect(enzymeWrapper.props().showDataMessage).to.equal(false);
    expect(enzymeWrapper.props().currentKey).to.equal('1')
  });

  it('find Page in Sidebar', () => {
    const wrapper = shallow(
        <Sidebar />
    );
    expect(wrapper.find(Page)).to.have.length(1);
  });

  it('simulate click event on Sidebar', () => {
    const onTemplateClick = sinon.spy();
    const wrapper = shallow(<Sidebar showSelected={onTemplateClick} />)
    wrapper.find('.template').simulate('click');
    expect(onTemplateClick).to.have.property('callCount', 1)
  });

  it('should dispatch an action if localStorage has value', () => {
    const value = true;
    expect(shipmntsActions.showDataMessage(value)).to.eql({ type: shipmntsTypes.SHOW_MESSAGE, value });
  });

  it('should dispatch an action if template is clicked', () => {
    const value = true;
    expect(shipmntsActions.isTemplateVisible(value)).to.eql({ type: shipmntsTypes.IS_TEMPLATE_VISIBLE, value });
  });

  it('should dispatch an action to set data', () => {
    const templateValue = { '1': 'XYZ Tech' };
    expect(shipmntsActions.setValueObj(templateValue)).to.eql({ type: shipmntsTypes.SET_VALUE_OBJ, templateValue });
  });

  it('should dispatch an action to update Key', () => {
    const value = '2';
    expect(shipmntsActions.updateCurrentKey(value)).to.eql({ type: shipmntsTypes.UPDATE_CURRENT_KEY, value });
  });

  it('should dispatch an action to set percent', () => {
    const value = 6;
    expect(shipmntsActions.setPercent(value)).to.eql({ type: shipmntsTypes.SET_PERCENT, value });
  });
})
