import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { LocaleProvider } from 'antd';
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga'
import shipmntsApp from './reducers';
import rootSaga from './sagas';
import enUS from 'antd/lib/locale-provider/en_US';
import App from './containers/App';
import './index.css';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
  shipmntsApp,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(sagaMiddleware)
);

sagaMiddleware.run(rootSaga);

ReactDOM.render(
  <Provider store={store}>
    <LocaleProvider locale={enUS}>
      <App />
    </LocaleProvider>
  </Provider>,
  document.getElementById('root')
);
