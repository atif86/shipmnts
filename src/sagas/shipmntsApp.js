import { put, select } from 'redux-saga/effects';

import { actions as shipmntsActions, selectors as shipmntsSelector } from '../reducers/shipmntsApp';

function* loadSaveData() {
  const getValue = localStorage.getItem('valueObj');
  yield put(shipmntsActions.setValueObj(JSON.parse(getValue)));
  yield put(shipmntsActions.showDataMessage(false));
  yield put(shipmntsActions.isTemplateVisible(true));
}

function* deleteSaveData() {
  localStorage.clear();
  yield put(shipmntsActions.setValueObj({}));
  yield put(shipmntsActions.showDataMessage(false));
  yield put(shipmntsActions.isTemplateVisible(true));
  yield put(shipmntsActions.updateCurrentKey('1'));
  yield put(shipmntsActions.setPercent(0));
}

function* showSelectedTemplate() {
  if(localStorage.getItem('valueObj') !== null){
    yield put(shipmntsActions.showDataMessage(true));
  } else {
    yield put(shipmntsActions.isTemplateVisible(true));
  }
}

function* exit() {
  const inputValue = yield select(shipmntsSelector.inputValue);
  if(inputValue !== ''){
    yield put(shipmntsActions.setInputValue(''));
  }
  yield put(shipmntsActions.isTemplateVisible(false));
}

function* setData(action) {
  const data = action.value;
  const templateValue = yield select(shipmntsSelector.valueObj);
  const currentKey = yield select(shipmntsSelector.currentKey);
  const duplicateValue = yield select(shipmntsSelector.duplicateValue);
  let newerTemplateValue = {};

  if(currentKey === duplicateValue.get(0)) {
    newerTemplateValue = templateValue.set(duplicateValue.get(0), data);
    newerTemplateValue = newerTemplateValue.set(duplicateValue.get(1), data);
  } else {
    newerTemplateValue = templateValue.set(`${currentKey}`, data)
  }

  localStorage.setItem('valueObj', JSON.stringify(newerTemplateValue));
  yield put(shipmntsActions.setValueObj(newerTemplateValue));
  yield put(shipmntsActions.setInputValue(data));
}

function* updateKey(action) {
  const data = action.value;
  const template = yield select(shipmntsSelector.template);
  const valueObj = yield select(shipmntsSelector.valueObj);
  const duplicateValue = yield select(shipmntsSelector.duplicateValue);
  let percent = yield select(shipmntsSelector.percentComplete);
  let updateKey = '';

  const arrayOfKeys = template.keySeq().toArray();
  const arrayLength = arrayOfKeys.length;
  const sortedArray = arrayOfKeys.sort((a,b) => a - b);
  const positionOfElement = sortedArray.indexOf(data);

  if(arrayLength !== positionOfElement + 1) {
    if(sortedArray[positionOfElement + 1] === duplicateValue.get(1)){
      updateKey = sortedArray[positionOfElement + 2];
      percent += 100 / arrayLength;
    } else {
      updateKey = sortedArray[positionOfElement + 1]
      percent += sortedArray[positionOfElement] === duplicateValue.get(0) ? ( 100 / arrayLength ) * duplicateValue.size : 100 / arrayLength;
    }
  } else {
    updateKey = '1'
    percent += 100 / arrayLength;
  }

  const getValue = valueObj.get(updateKey);
  if(getValue !== undefined){
    yield put(shipmntsActions.setInputValue(getValue));
    if(updateKey === '1') {
      yield put(shipmntsActions.setPercent(percent));
    }
  } else {
    yield put(shipmntsActions.setInputValue(''));
    yield put(shipmntsActions.setPercent(percent));
  }

  yield put(shipmntsActions.updateCurrentKey(updateKey));
}

function* gotoSelectedKey(action) {
  const { key } = action;
  const valueObj = yield select(shipmntsSelector.valueObj);
  const getValue = valueObj.get(key);
  if(getValue !== undefined){
    yield put(shipmntsActions.setInputValue(getValue));
  } else {
    yield put(shipmntsActions.setInputValue(''));
  }
  yield put(shipmntsActions.updateCurrentKey(key));
}

export const sagas = {
  loadSaveData,
  deleteSaveData,
  showSelectedTemplate,
  exit,
  setData,
  updateKey,
  gotoSelectedKey,
};
