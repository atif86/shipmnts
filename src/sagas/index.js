import { takeEvery } from 'redux-saga/effects';

import { types as shipmntsTypes } from '../reducers/shipmntsApp';
import { sagas as shipmntsSagas } from './shipmntsApp';

export default function* rootSaga() {
  yield [
    takeEvery(shipmntsTypes.LOAD_SAVE_DATA, shipmntsSagas.loadSaveData),
    takeEvery(shipmntsTypes.DELETE_SAVE_DATA, shipmntsSagas.deleteSaveData),
    takeEvery(shipmntsTypes.SHOW_SELECTED_TEMPLATE, shipmntsSagas.showSelectedTemplate),
    takeEvery(shipmntsTypes.EXIT, shipmntsSagas.exit),
    takeEvery(shipmntsTypes.SET_DATA, shipmntsSagas.setData),
    takeEvery(shipmntsTypes.UPDATE_KEY, shipmntsSagas.updateKey),
    takeEvery(shipmntsTypes.GOTO_SELECTED_KEY, shipmntsSagas.gotoSelectedKey),
  ];
}
