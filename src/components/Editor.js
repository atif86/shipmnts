import React from 'react';
import { Modal } from 'antd';
import { connect } from 'react-redux';
import InputDetails from './InputDetails';
import ActionButton from './ActionButton';
import FormPreview from './FormPreview';
import Page from './Page';

import { actions as shipmntsAction } from '../reducers/shipmntsApp';

class Editor extends React.Component{
  render() {
    return (
      <div className="full-height">
        <InputDetails />
        <ActionButton
          downloadPdf={this.props.downloadPdf}
        />
        <FormPreview
          openModal={this.props.openModal}
          valueObj={this.props.valueObj}
          gotoKey={this.props.gotoKey}
        />
        {this.props.openModal &&
          <Modal
            visible={this.props.openModal}
            onCancel={(value) => this.props.togglePreview(false)}
            closable={false}
            footer={null}
            width="40%"
          >
            <Page
              sidebar={false}
              openModal={this.props.openModal}
              valueObj={this.props.valueObj}
              gotoKey={this.props.gotoKey}
            />
          </Modal>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  openModal: state.shipmntsApp.get('openModal'),
  valueObj: state.shipmntsApp.get('valueObj'),
})

const mapDispatchToProps = (dispatch) => ({
  togglePreview: (value) => dispatch(shipmntsAction.onTogglePreview(value)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Editor);
