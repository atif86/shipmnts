import React from 'react';
import { Button, Progress } from 'antd';
import { connect } from 'react-redux';

import { actions as shipmntsAction } from '../reducers/shipmntsApp';

class ActionButton extends React.Component{

  _clickExit = () => {
    this.props.exit()
  }

  _clickPreview = () => {
    this.props.togglePreview(true);
  }

  _clickDownload = () => {
    this.props.downloadPdf()
  }

  render() {
    return (
      <div className="action-container">
        <div style={{ width: '50%', marginLeft: 30, marginRight: '25%' }}>
          <Progress percent={Math.ceil(this.props.percentComplete)} />
        </div>
        <div style={{ display: 'flex', justifyContent: 'flex-end', paddingRight: 30 }}>
          <Button type="primary" icon="download" size="large" onClick={() => this._clickDownload()} className="action-btn">
            Download
          </Button>
          <Button type="primary" icon="eye" size="large" onClick={() => this._clickPreview()} className="action-btn">Preview</Button>
          <Button type="primary" icon="save" size="large" onClick={() => this._clickExit()} className="action-btn">Exit</Button>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  percentComplete: state.shipmntsApp.get('percentComplete'),
})

const mapDispatchToProps = (dispatch) => ({
  exit: () => dispatch(shipmntsAction.exit()),
  togglePreview: (value) => dispatch(shipmntsAction.onTogglePreview(value)),
})

export default connect(mapStateToProps, mapDispatchToProps)(ActionButton);
