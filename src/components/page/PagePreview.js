import React from 'react';

const PagePreview = props => (
  <div className="page-preview" style={{ border: '2px #000' }}>
    <div style={{ height: '40%', borderBottom: '1px solid', display: 'flex' }}>
      <div style={{ width: '50%', borderRight: '1px solid', height: '100%' }}>
        <div style={{ height: '25%', borderBottom: '1px solid'}} />
        <div style={{ height: '25%', borderBottom: '1px solid'}} />
      </div>
      <div style={{ width: '50%', height: '100%' }}>
        <div style={{ height: '50%', borderBottom: '1px solid'}} />
      </div>
    </div>
    <div style={{ height: '35%', borderBottom: '1px solid', display: 'flex'}}>
    </div>
    <div style={{ height: '25%' }}>
      <div style={{ width: '50%', borderRight: '1px solid', height: '100%' }}>
      </div>
    </div>
  </div>
)

export default PagePreview;
