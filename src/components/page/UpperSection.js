import React from 'react';

const UpperSection = props => (
  <div className="upper-section">
    <div className="right-border half-width">
      <div style={{ height: '60%', borderBottom: '1px solid' }}>
        <div style={{ height: '50%', borderBottom: '1px solid' }}>
          <p className="section-heading">1.Goods consigned from (Exporter`s business name, address,country)</p>
          {props.valueObj && props.valueObj.get('1') ?
            <p className="section-value" onClick={() => props.gotoKey('1')}>{props.valueObj.get('1')}</p>
            :
            <p
              className="section-before-value"
              style={{ visibility: props.openModal ? 'hidden' : 'visible' }}
              onClick={() => props.gotoKey('1')}
            >
              Exporters business name
            </p>
          }
          {props.valueObj && props.valueObj.get('2') ?
            <p className="section-value" onClick={() => props.gotoKey('2')}>{props.valueObj.get('2')}</p>
            :
            <p
              className="section-before-value"
              style={{ visibility: props.openModal ? 'hidden' : 'visible' }}
              onClick={() => props.gotoKey('2')}
            >
              Exporters address, country
            </p>
          }
        </div>
        <div className="half-height">
          <p className="section-heading">2.Goods consigned to (consignee`s, name, address, country)</p>
          {props.valueObj && props.valueObj.get('3') ?
           <p className="section-value" onClick={() => props.gotoKey('3')}>{props.valueObj.get('3')}</p>
           :
           <p
             className="section-before-value"
             style={{ visibility: props.openModal ? 'hidden' : 'visible' }}
             onClick={() => props.gotoKey('3')}
            >
              Consignee name
            </p>
          }
          {props.valueObj && props.valueObj.get('4') ?
            <p className="section-value" onClick={() => props.gotoKey('4')}>{props.valueObj.get('4')}</p>
            :
            <p
              className="section-before-value"
              style={{ visibility: props.openModal ? 'hidden' : 'visible' }}
              onClick={() => props.gotoKey('4')}
            >
              Consignee address, country
            </p>
          }
        </div>
      </div>
      <div style={{ height: '40%' }}>
        <p className="section-heading">3.Means of transport and route (as far as known)</p>
        {props.valueObj && props.valueObj.get('5') ?
          <p className="section-value" onClick={() => props.gotoKey('5')}>{props.valueObj.get('5')}</p>
          :
          <p
            className="section-before-value"
            style={{ visibility: props.openModal ? 'hidden' : 'visible' }}
            onClick={() => props.gotoKey('5')}
          >
            Means of Transport
          </p>
        }
        {props.valueObj && props.valueObj.get('6') ?
          <p className="section-value" onClick={() => props.gotoKey('6')}>{props.valueObj.get('6')}</p>
          :
          <p
            className="section-before-value"
            style={{ visibility: props.openModal ? 'hidden' : 'visible' }}
            onClick={() => props.gotoKey('6')}
          >
            Port of loading
          </p>
        }
        {props.valueObj && props.valueObj.get('7') ?
          <p className="section-value" onClick={() => props.gotoKey('7')}>{props.valueObj.get('7')}</p>
          :
          <p
            className="section-before-value"
            style={{ visibility: props.openModal ? 'hidden' : 'visible' }}
            onClick={() => props.gotoKey('7')}
          >
            Port of discharge
          </p>
        }
      </div>
    </div>
    <div className="half-width">
      <div style={{ height: '60%', borderBottom: '1px solid' }}>
        <div style={{ paddingTop: 10, paddingLeft: 10, fontSize: 16, display: 'flex' }}>Reference No.
          <p style={{ fontSize: 14, fontWeight: 'bold', marginLeft: 20 }}>SA 0 0 0 2 2 7 3 2</p>
        </div>
        <p style={{ fontSize: 14, fontWeight: 'bold', height: '20%', display: 'flex', alignItems: 'center', justifyContent: 'flex-end' }}>
          SAARC PREFERENTIAL TRADING ARRANGEMENT
        </p>
        <p style={{ fontSize: 14, fontWeight: 'bold', display: 'flex', justifyContent: 'center', marginLeft: '15%' }}>(SAPTA)</p>
        <p style={{ fontSize: 16, display: 'flex', justifyContent: 'center', marginLeft: '15%' }}>(Combined declaration and certificate)</p>
        <div style={{ paddingLeft: 10, display: 'flex', marginTop: 15 }}>
          <p style={{  fontSize: 14 }}>Issued in</p>
          <div style={{ width: '65%', marginLeft: '8%' }}>
            <p style={{ fontSize: 18, fontWeight: 'bold', display: 'flex', justifyContent: 'center' }}>INDIA</p>
            <hr />
            <p style={{ fontSize: 16, display: 'flex', justifyContent: 'center' }}>(Country)</p>
          </div>
        </div>
      </div>
      <div style={{ height: '40%' }}>
        <p className="section-heading">4.For official use</p>
      </div>
    </div>
  </div>
)

export default UpperSection;
