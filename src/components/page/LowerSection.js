import React from 'react';

const LowerSection = props => (
  <div className="lower-section">
    <div className="right-border half-width" >
      <p className="section-big-heading">11.Declaration by the exporter</p>
      <p className="para">The undersigned hereby declares that the above details and statements are correct; that all the goods were produced in</p>
      <div style={{ width: '90%', paddingLeft: '10%' }}>
        <div style={{ fontSize: 16, fontWeight: 'bold', display: 'flex', justifyContent: 'center', height: 30 }}>
          {props.valueObj && props.valueObj.get('17') && <p className="section-big-value">{props.valueObj.get('17')}</p>}
        </div>
        <hr />
        <p style={{ fontSize: 13, display: 'flex', justifyContent: 'center' }}>(Country)</p>
      </div>
      <p className="para" style={{ paddingTop: 10 }}>and that they comply with the origin requirements specified for those goods in SAPTA for goods exported to</p>
      <div style={{ width: '90%', paddingLeft: '10%' }}>
        <div style={{ fontSize: 16, fontWeight: 'bold', display: 'flex', justifyContent: 'center', height: 30 }}>
          {props.valueObj && props.valueObj.get('18') && <p className="section-big-value">{props.valueObj.get('18')}</p>}
        </div>
        <hr />
        <p style={{ fontSize: 13, display: 'flex', justifyContent: 'center' }}>(Importing Country)</p>
      </div>
      <div style={{ width: '90%', paddingLeft: '10%', paddingTop: '8%' }}>
        <hr />
        <p style={{ fontSize: 10, display: 'flex', justifyContent: 'center' }}>Place and date,signature and stamp of authorized signatory</p>
      </div>
    </div>
    <div className="half-width">
      <p className="section-big-heading">12.Certification</p>
      <p className="para">It is hereby certified, on the basis of control carried out,that the declaration by the exporter is correct.</p>
      <div style={{ width: '90%', paddingLeft: '10%', paddingTop: '60%' }}>
        <hr />
        <p style={{ fontSize: 10, display: 'flex', justifyContent: 'center' }}>Place and date,signature and stamp of certifying authority</p>
      </div>
    </div>
  </div>
)

export default LowerSection;
