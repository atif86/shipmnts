import React from 'react';

const MiddleSection = props => (
  <div className="middle-section">
    <div className="right-border" style={{ width: '10%' }}>
      <p className="section-heading">5.Tariff item Number</p>
      {props.valueObj && props.valueObj.get('8') ?
       <p className="section-value" onClick={() => props.gotoKey('8')}>{props.valueObj.get('8')}</p>
       :
       <p
         className="section-before-value"
         style={{ visibility: props.openModal ? 'hidden' : 'visible' }}
         onClick={() => props.gotoKey('8')}
        >
          HS Code
        </p>
      }
    </div>
    <div className="right-border" style={{ width: '15%' }}>
      <p className="section-heading">6.Marks and number of packages</p>
      {props.valueObj && props.valueObj.get('9') ?
        <p className="section-value" onClick={() => props.gotoKey('9')}>{props.valueObj.get('9')}</p>
        :
        <p
          className="section-before-value"
          style={{ visibility: props.openModal ? 'hidden' : 'visible' }}
          onClick={() => props.gotoKey('9')}
        >
          Marks
        </p>
      }
      {props.valueObj && props.valueObj.get('10') ?
        <p className="section-value" onClick={() => props.gotoKey('10')}>{props.valueObj.get('10')}</p>
        :
        <p
          className="section-before-value"
          style={{ visibility: props.openModal ? 'hidden' : 'visible' }}
          onClick={() => props.gotoKey('10')}
        >
            Number of Packages
        </p>
      }
    </div>
    <div className="right-border" style={{ width: '35%' }}>
      <p className="section-heading">7.Number and kind of packages, description of goods</p>
      {props.valueObj && props.valueObj.get('11') ?
        <p className="section-value" onClick={() => props.gotoKey('11')}>{props.valueObj.get('11')}</p>
        :
        <p
          className="section-before-value"
          style={{ visibility: props.openModal ? 'hidden' : 'visible' }}
          onClick={() => props.gotoKey('11')}
        >
          Type of Package
        </p>
      }
      {props.valueObj && props.valueObj.get('12') ?
        <p className="section-value" onClick={() => props.gotoKey('12')}>{props.valueObj.get('12')}</p>
        :
        <p
          className="section-before-value"
          style={{ visibility: props.openModal ? 'hidden' : 'visible' }}
          onClick={() => props.gotoKey('12')}
        >
          Description of goods
        </p>
      }
    </div>
    <div className="right-border" style={{ width: '15%' }}>
      <p className="section-heading">8.Origin criterion(see notes overleaf)</p>
      {props.valueObj && props.valueObj.get('13') ?
       <p className="section-value" onClick={() => props.gotoKey('13')}>{props.valueObj.get('13')}</p>
       :
       <p
         className="section-before-value"
         style={{ visibility: props.openModal ? 'hidden' : 'visible' }}
         onClick={() => props.gotoKey('13')}
        >
          Origin Country
        </p>
      }
    </div>
    <div className="right-border" style={{ width: '10%' }}>
      <p className="section-heading">9.Gross weight or other quantity</p>
      {props.valueObj && props.valueObj.get('14') ?
       <p className="section-value" onClick={() => props.gotoKey('14')}>{props.valueObj.get('14')}</p>
       :
       <p
         className="section-before-value"
         style={{ visibility: props.openModal ? 'hidden' : 'visible' }}
         onClick={() => props.gotoKey('14')}
        >
          Gross Weight
        </p>
      }
    </div>
    <div style={{ width: '15%' }}>
      <p className="section-heading">10.Number and date of invoices</p>
      {props.valueObj && props.valueObj.get('15') ?
        <p className="section-value" onClick={() => props.gotoKey('15')}>{props.valueObj.get('15')}</p>
        :
        <p
          className="section-before-value"
          style={{ visibility: props.openModal ? 'hidden' : 'visible' }}
          onClick={() => props.gotoKey('15')}
        >
          Invoice Number
        </p>
      }
      {props.valueObj && props.valueObj.get('16') ?
        <p className="section-value" onClick={() => props.gotoKey('16')}>{props.valueObj.get('16')}</p>
        :
        <p
          className="section-before-value"
          style={{ visibility: props.openModal ? 'hidden' : 'visible' }}
          onClick={() => props.gotoKey('16')}
        >
          Date of invoice
        </p>
      }
    </div>
  </div>
)

export default MiddleSection;
