import React from 'react';
import UpperSection from './page/UpperSection';
import MiddleSection from './page/MiddleSection';
import LowerSection from './page/LowerSection';
import PagePreview from './page/PagePreview';

const Page = props => (
  <div style={{ width: '100%' }}>
    {!props.sidebar ?
      <div id="page" className="page">
        <UpperSection
          openModal={props.openModal}
          valueObj={props.valueObj}
          gotoKey={props.gotoKey}
        />
        <MiddleSection
          openModal={props.openModal}
          valueObj={props.valueObj}
          gotoKey={props.gotoKey}
        />
        <LowerSection
          valueObj={props.valueObj}
        />
      </div>
      :
      <PagePreview />
    }
  </div>
)

export default Page;
