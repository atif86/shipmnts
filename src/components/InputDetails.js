import React from 'react';
import { connect } from 'react-redux';
import { Input, Button, message, DatePicker } from 'antd';

import { actions as shipmntsAction } from '../reducers/shipmntsApp';

class InputDetails extends React.Component{

  onChangeInput = (e, title) => {
    const { setData } = this.props;
    const isDate = title.indexOf(title.match(/date/i));
    const value =  isDate >= 0 ? e.format("DD-MM-YYYY") : e.target.value;
    let data = '';
    const isNumber = title.indexOf(title.match(/number/i));
    if(isNumber === -1) {
      data = value;
      setData(data);
    }

    if(isNumber >= 0 && this.isNumeric(value)) {
      data = value;
      setData(data);
    } else if(isNumber >= 0 && !this.isNumeric(value)) {
      message.error('Enter numbers current field is numeric')
    }

    if(isDate >= 0) {
      data = value;
      setData(data);
    }
  }

  isNumeric = (data) => {
    return !isNaN(parseFloat(data)) && isFinite(data);
  }

  updateKey = () => {
    const { updateKey } = this.props;
    if(this.props.inputValue !== ''){
      updateKey(this.props.currentKey);
    }
  }

  render() {
    const placeValue = this.props.template.get(this.props.currentKey);
    return (
      <div className="input-container">
        <p style={{ fontSize: 18, fontWeight: 'bold', paddingRight: 10 }}>Step: {this.props.currentKey}</p>
        {placeValue.indexOf(placeValue.match(/date/i)) >= 0 ?
          <DatePicker
            placeholder={this.props.template.get(this.props.currentKey)}
            onChange={(value, title) => this.onChangeInput(value, this.props.template.get(this.props.currentKey))}
            style={{ width: '60%', height: '22%', borderColor: '#c0392b', marginRight: 10, marginLeft: 10 }}
          />
          :
          <Input
            id='input'
            className="inputBox"
            placeholder={this.props.template.get(this.props.currentKey)}
            value={this.props.inputValue}
            onChange={(value, title) => this.onChangeInput(value, this.props.template.get(this.props.currentKey))}
            onPressEnter={() => this.updateKey()}
          />}
        <Button onClick={() => this.updateKey()} icon="arrow-right" className="next-btn">Next</Button>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  template: state.shipmntsApp.get('template'),
  inputValue: state.shipmntsApp.get('inputValue'),
  currentKey: state.shipmntsApp.get('currentKey'),
  currentChildKey: state.shipmntsApp.get('currentChildKey'),
})

const mapDispatchToProps = dispatch => ({
  updateKey: (value) => dispatch(shipmntsAction.updateKey(value)),
  setData: (value) => dispatch(shipmntsAction.setData(value)),
})

export default connect(mapStateToProps, mapDispatchToProps)(InputDetails);
