import React from 'react';
import Page from './Page';

class FormPreview extends React.Component{
  render() {
    return (
      <div className="preview-container">
        <Page
          sidebar={false}
          valueObj={this.props.valueObj}
          gotoKey={this.props.gotoKey}
        />
      </div>
    )
  }
}

export default FormPreview;
