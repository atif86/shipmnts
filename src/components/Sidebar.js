import React from 'react';
import Page from './Page';

class Sidebar extends React.Component{

  _templateSelected = () => {
    this.props.showSelected();
  }

  render() {
    return (
      <div className="sidebar">
        <div className="template" style={{ paddingTop: 30 }} onClick={() => this._templateSelected()}>
          <Page sidebar={true} />
        </div>
        <p style={{ fontSize: 16, fontStyle: 'italic', color: this.props.showTemplate ? '#c0392b' : '#fff' , textAlign: 'center' }}>(SAARC Template)</p>
      </div>
    )
  }
}

export default Sidebar;
