import React, { Component } from 'react';
import { connect } from 'react-redux';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import $ from 'jquery';
import { Row, Col, Modal } from 'antd';
import Sidebar from '../components/Sidebar';
import Editor from '../components/Editor';
import './App.css';

import { actions as shipmntsAction } from '../reducers/shipmntsApp';

class App extends Component {

  componentDidMount() {
    const { showMessage, template, setDuplicateValues } = this.props;
    if(localStorage.getItem('valueObj') !== null){
      showMessage(true)
    }

    const arrayOfValues = template.valueSeq().toArray();
    const sorted_arr = arrayOfValues.slice().sort();
    const result = [];
    for (let i = 0; i < arrayOfValues.length - 1; i++) {
      if (sorted_arr[i + 1] === sorted_arr[i]) {
          result.push(sorted_arr[i]);
      }
    }

    const getDulicateValue = template.filter(key => key === result[0]);
    const arrOfDuplicateValue = getDulicateValue.keySeq().toArray();
    setDuplicateValues(arrOfDuplicateValue);
  }

  _downloadPdf = () => {
    this.getCanvas()
    .then((canvas) => {
      const img = canvas.toDataURL('image/png')
      const doc = new jsPDF({ unit: 'px', format: 'a4' });
      doc.addImage(img, 'JPEG', 20, 20);
      doc.save('template.pdf');
    })
  }

  getCanvas = () => {
    const page = $('.page');
    return html2canvas(page, {
      imageTimeout: 2000,
      removeContainer: true
    });
  }

  render() {
    return (
      <Row id="parent" className="full-height">
        <Col span={4} className="full-height">
          <Sidebar
            showTemplate={this.props.showTemplate}
            showSelected={this.props.showSelectedTemplate}
          />
        </Col>
        <Col span={20} className="full-height">
          {this.props.showTemplate ?
            <Editor
              currentKey={this.props.currentKey}
              downloadPdf={() => this._downloadPdf()}
              gotoKey={(key, childKey) => this.props.gotoSelectedKey(key, childKey)}
            />
            :
            <div style={{ display: 'flex', height: '100vh', alignItems: 'center', justifyContent: 'center' }}>
              <p style={{ fontSize: 20, fontWeight: 'bold', color: '#95a5a6'}}>Select template from left-side to continue</p>
            </div>
          }
        </Col>
        {
          this.props.showDataMessage &&
          <Modal
            visible={this.props.showDataMessage}
            title="Dear User!!"
            okText="Yes"
            cancelText="No"
            onOk={this.props.loadSaveData}
            onCancel={this.props.deleteSaveData}
          >
            You have unsaved changes do you want to restore it?
          </Modal>
        }
      </Row>
    );
  }
}

const mapStateToProps = state => ({
  template: state.shipmntsApp.get('template'),
  showTemplate: state.shipmntsApp.get('showTemplate'),
  showDataMessage: state.shipmntsApp.get('showDataMessage'),
  currentKey: state.shipmntsApp.get('currentKey'),
});

const mapDispatchToProps = dispatch => ({
  isTemplateVisible: (value) => dispatch(shipmntsAction.isTemplateVisible(value)),
  setValueObj: (templateValue) => dispatch(shipmntsAction.setValueObj(templateValue)),
  showMessage: (value) => dispatch(shipmntsAction.showDataMessage(value)),
  loadSaveData: () => dispatch(shipmntsAction.loadSaveData()),
  deleteSaveData: () => dispatch(shipmntsAction.deleteSaveData()),
  showSelectedTemplate: () => dispatch(shipmntsAction.showSelectedTemplate()),
  gotoSelectedKey: (key, childKey) => dispatch(shipmntsAction.gotoSelectedKey(key, childKey)),
  setDuplicateValues: (value) => dispatch(shipmntsAction.setDuplicateValues(value)),
})

export default connect(mapStateToProps, mapDispatchToProps)(App);
