import { combineReducers } from 'redux';

import shipmntsApp from './shipmntsApp';

export default combineReducers({
  shipmntsApp,
});
