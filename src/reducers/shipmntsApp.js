import { fromJS } from 'immutable';

// Actions
export const types = {
  INITIAL: 'app/shipmntsApp/INITIAL',
  ON_TOGGLE_PREVIEW: 'app/shipmntsApp/ON_TOGGLE_PREVIEW',
  IS_TEMPLATE_VISIBLE: 'app/shipmntsApp/IS_TEMPLATE_VISIBLE',
  SET_VALUE_OBJ: 'app/shipmntsApp/SET_VALUE_OBJ',
  SHOW_MESSAGE: 'app/shipmntsApp/SHOW_MESSAGE',
  LOAD_SAVE_DATA: 'app/shipmntsApp/LOAD_SAVE_DATA',
  DELETE_SAVE_DATA: 'app/shipmntsApp/DELETE_SAVE_DATA',
  SHOW_SELECTED_TEMPLATE: 'app/shipmntsApp/SHOW_SELECTED_TEMPLATE',
  EXIT: 'app/shipmntsApp/EXIT',
  SET_INPUT_VALUE: 'app/shipmntsApp/SET_INPUT_VALUE',
  SET_DATA: 'app/shipmntsApp/SET_DATA',
  UPDATE_KEY: 'app/shipmntsApp/UPDATE_KEY',
  UPDATE_CURRENT_KEY: 'app/shipmntsApp/UPDATE_CURRENT_KEY',
  SET_PERCENT: 'app/shipmntsApp/SET_PERCENT',
  GOTO_SELECTED_KEY: 'app/shipmntsApp/GOTO_SELECTED_KEY',
  SET_DUPLICATE_VALUE: 'app/shipmntsApp/SET_DUPLICATE_VALUE'
}

// Reducer
export const initialState = fromJS({
  template: {
    '1': 'Exporters Company Name',
    '2': 'Exporters Company Address',
    '3': 'Consignee Company Name',
    '4': 'Consignee Company Address',
    '5': 'Transport Mode',
    '6': 'Port of Loading',
    '7': 'Port of Discharge',
    '8': 'HS Code',
    '9': 'Marks',
    '10': 'Number of Packages',
    '11': 'Type of Packages',
    '12': 'Description of Goods',
    '13': 'Origin Country',
    '14': 'Gross Weight',
    '15': 'Invoice Number',
    '16': 'Invoice Date',
    '17': 'Origin Country',
    '18': 'Importing Country',
  },
  openModal: false,
  showTemplate: false,
  valueObj: {},
  showDataMessage: false,
  inputValue: '',
  currentKey: '1',
  arrayOfKeys: [],
  percentComplete: 0,
  duplicateValue: [],
});

export default (state = initialState, action) => {
  switch (action.type) {
    case types.INITIAL:
      return initialState;
    case types.ON_TOGGLE_PREVIEW:
      return state
        .set('openModal', action.value);
    case types.IS_TEMPLATE_VISIBLE:
      return state
        .set('showTemplate', action.value);
    case types.SET_VALUE_OBJ:
      return state
       .set('valueObj', fromJS(action.templateValue));
    case types.SHOW_MESSAGE:
      return state
        .set('showDataMessage', action.value );
    case types.SET_INPUT_VALUE:
      return state
        .set('inputValue', action.value );
    case types.UPDATE_CURRENT_KEY:
      return state
        .set('currentKey', action.value );
    case types.SET_PERCENT:
      return state
        .set('percentComplete', action.value );
    case types.SET_DUPLICATE_VALUE:
      return state
        .set('duplicateValue', fromJS(action.value));
    default:
      return state;
  }
};

// Action Creators
export const actions = {
  init: () => ({ type: types.INITIAL }),
  onTogglePreview: (value) => ({ type: types.ON_TOGGLE_PREVIEW, value }),
  isTemplateVisible: (value) => ({ type: types.IS_TEMPLATE_VISIBLE, value }),
  setValueObj: (templateValue) => ({ type: types.SET_VALUE_OBJ, templateValue }),
  showDataMessage: (value) => ({ type: types.SHOW_MESSAGE, value }),
  loadSaveData: () => ({ type: types.LOAD_SAVE_DATA }),
  deleteSaveData: () => ({ type: types.DELETE_SAVE_DATA }),
  showSelectedTemplate: () => ({ type: types.SHOW_SELECTED_TEMPLATE }),
  exit: () => ({ type: types.EXIT }),
  setInputValue: (value) => ({ type: types.SET_INPUT_VALUE, value }),
  setData: (value) => ({ type: types.SET_DATA, value }),
  updateKey: (value) => ({ type: types.UPDATE_KEY, value }),
  updateCurrentKey: (value) => ({ type: types.UPDATE_CURRENT_KEY, value }),
  setPercent: (value) => ({ type: types.SET_PERCENT, value }),
  gotoSelectedKey: (key) => ({ type: types.GOTO_SELECTED_KEY, key }),
  setDuplicateValues: (value) => ({ type: types.SET_DUPLICATE_VALUE, value })
};

export const selectors = {
  template: (state) => state.shipmntsApp.get('template'),
  inputValue: (state) => state.shipmntsApp.get('inputValue'),
  valueObj: (state) => state.shipmntsApp.get('valueObj'),
  currentKey: (state) => state.shipmntsApp.get('currentKey'),
  percentComplete: (state) => state.shipmntsApp.get('percentComplete'),
  duplicateValue: (state) => state.shipmntsApp.get('duplicateValue')
};
